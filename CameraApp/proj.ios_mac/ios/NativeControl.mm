//
//  NativeControl.m
//


#import "NativeControl.h"
#import "AppController.h"
#import "RootViewController.h"

#import <Foundation/Foundation.h>
#import <UIKit/UIDevice.h>
#import <QuartzCore/QuartzCore.h>

USING_NS_CC;

static NativeControl* s_pOniHelper = NULL;
static UIImagePickerController *imagePickerController = NULL;


NativeControl::NativeControl() {
}

NativeControl::~NativeControl() {
    
    CC_SAFE_RELEASE_NULL(s_pOniHelper);
    
    if (imagePickerController) {
        [imagePickerController release];
        imagePickerController = nil;
    }
}

NativeControl* NativeControl::sharedInstance() {
    
    if (s_pOniHelper == NULL){
        s_pOniHelper = new NativeControl();
        s_pOniHelper->init();
    }
    
    return s_pOniHelper;
}

bool NativeControl::init() {
    return true;
}



#pragma mark -
#pragma mark ImagePicker

void NativeControl::showImagePicker(void) {
    
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        AppController *appController = (AppController *)[[UIApplication sharedApplication] delegate];
        
        imagePickerController = [[UIImagePickerController alloc] init];
        imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
        imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePickerController.videoQuality = UIImagePickerControllerQualityTypeMedium;
        imagePickerController.delegate = (id)appController.viewController;
        
        [appController.viewController presentViewController:imagePickerController animated:YES completion:Nil];
    }
}




