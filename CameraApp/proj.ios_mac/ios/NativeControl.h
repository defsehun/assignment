
#ifndef  _NativeControl_H_
#define  _NativeControl_H_
#include "cocos2d.h"

NS_CC_BEGIN

class CC_DLL NativeControl : public CCObject {
    
public:
	NativeControl();
	virtual ~NativeControl();
    
    static NativeControl* sharedInstance();
    bool init();
    
    void showImagePicker();
    
private:
    
};

NS_CC_END

#endif