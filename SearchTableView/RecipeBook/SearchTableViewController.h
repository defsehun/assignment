//
//  SearchTableViewController.h
//
//  Created by Sehun Park on 28/1/16.
//  Copyright (c) 2016 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTableViewController : UIViewController <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) IBOutlet UITableView *tableView;

@end
