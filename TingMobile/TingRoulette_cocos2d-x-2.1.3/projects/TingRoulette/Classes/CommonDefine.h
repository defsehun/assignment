//
//  CommonDefine.h
//  TingRoulette
//
//  Created by Park Se Hun on 13. 8. 21..
//
//

#ifndef TingRoulette_CommonDefine_h
#define TingRoulette_CommonDefine_h

// APP Version
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
#define APP_MAJOR_VERSION 1
#define APP_MINOR_VERSION 0
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
#define APP_MAJOR_VERSION 1
#define APP_MINOR_VERSION 0
#endif

// define macro
#define SERVER_URL      "182.161.114.155" //dev
//#define SERVER_URL      "localhost"

#define ResourcePathMake(str1, str2) CCString::createWithFormat("%s%s", str1, str2)->getCString()
#define ServerUrlMake(functionName) CCString::createWithFormat("https://%s:3003/%s/", SERVER_URL, functionName)->getCString()
#define CCCA(x)   (x->copy()->autorelease())

// CCB folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const CCB_FOLDER_PATH = "assets/CCBFiles/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const CCB_FOLDER_PATH = "CCBFiles/";
#endif

#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const COMMON_IMAGE_PATH = "assets/Images/Common/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const COMMON_IMAGE_PATH = "Images/Common/";
#endif

// Roulette folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const rouletteImagePath = "assets/Images/Roulette/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const rouletteImagePath = "Images/Roulette/";
#endif

// Polaroid folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const polaroidImagePath = "assets/Images/Polaroid/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const polaroidImagePath = "Images/Polaroid/";
#endif

// RouletteResult folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const rouletteResultImagePath = "assets/Images/RouletteResult/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const rouletteResultImagePath = "Images/RouletteResult/";
#endif

// Title folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const TITLE_IMAGE_PATH = "assets/Images/Title/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const TITLE_IMAGE_PATH = "Images/Title/";
#endif










// json folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const GAME_DATA_PATH = "assets/GameData/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const GAME_DATA_PATH = "GameData/";
#endif

// sound folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const SOUND_FOLDER_PATH = "assets/Sound/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const SOUND_FOLDER_PATH = "Sound/";
#endif

// font folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const FONT_FOLDER_PATH = "assets/Fonts/BMFont/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const FONT_FOLDER_PATH = "Fonts/BMFont/";
#endif

// characters folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const CHARACTER_IMAGE_PATH = "assets/Images/Characters/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const CHARACTER_IMAGE_PATH = "Images/Characters/";
#endif

// characters folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const CONFIG_IMAGE_PATH = "assets/Images/ConfigLayer/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const CONFIG_IMAGE_PATH = "Images/ConfigLayer/";
#endif

// Indicators folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const INDICATOR_IMAGE_PATH = "assets/Images/Indicators/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const INDICATOR_IMAGE_PATH = "Images/Indicators/";
#endif

// Main folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const MAIN_IMAGE_PATH = "assets/Images/Main/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const MAIN_IMAGE_PATH = "Images/Main/";
#endif

// Ranking folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const RANKING_IMAGE_PATH = "assets/Images/Ranking/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const RANKING_IMAGE_PATH = "Images/Ranking/";
#endif

// Ready folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const READY_IMAGE_PATH = "assets/Images/Ready/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const READY_IMAGE_PATH = "Images/Ready/";
#endif

// Result folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const RESULT_IMAGE_PATH = "assets/Images/Result/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const RESULT_IMAGE_PATH = "Images/Result/";
#endif

// Shop folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const SHOP_IMAGE_PATH = "assets/Images/Shop/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const SHOP_IMAGE_PATH = "Images/Shop/";
#endif

// UI folder path
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const UI_IMAGE_PATH = "assets/Images/UI/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const UI_IMAGE_PATH = "Images/UI/";
#endif

// Toutorial
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const TUTORIAL_IMAGE_PATH = "assets/Images/Tutorial/";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const TUTORIAL_IMAGE_PATH = "Images/Tutorial/";
#endif

// font
#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
static const char* const FONT_NAME                      = "NanumGothicExtraBold";
#endif
#if (CC_TARGET_PLATFORM == CC_PLATFORM_ANDROID)
static const char* const FONT_NAME                      = "Fonts/NanumGothicExtraBold.ttf";
#endif

#define NORMAL_FONT_COLOR                                ccc3(88,62,47)

// game value
#define kMaxPhotoCount      8



#endif



