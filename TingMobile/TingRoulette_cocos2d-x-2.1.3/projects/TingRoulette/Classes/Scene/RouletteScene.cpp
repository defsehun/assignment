#include "RouletteScene.h"
#include "CommonDefine.h"
#include "RoulettePanLayer.h"
#include "CommonUtils.h"
#include "RouletteResultLayer.h"
#include "TitleScene.h"

#define zOderPanLayer       1001
#define zOderCharacter      1000
#define zOderPhoto          1000
#define zOderPolaroid       2000
#define zOderResultLayer    3000

#define zOderMenu           4000

#define kShowPhotoDelay     5

CCScene* RouletteScene::scene()
{
    CCScene *scene = CCScene::create();
    CCLayer *layer = RouletteScene::create();
    scene->addChild(layer);
    
    return scene;
}

bool RouletteScene::init()
{
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    setTouchEnabled(true);
    setTouchMode(kCCTouchesOneByOne);
    
    background = CCSprite::create(ResourcePathMake(rouletteImagePath, "gameview_back.PNG"));
    addChild(background);
    background->setPosition(ccp(background->getContentSize().width/2, background->getContentSize().height/2));
    
    RoulettePanLayer *panLayer = RoulettePanLayer::create();
    background->addChild(panLayer, zOderPanLayer);
    panLayer->setPosition(ccp(322, 648));
    
    CCMenuItemImage *menuItem = CCMenuItemImage::create(ResourcePathMake(rouletteImagePath, "menu_button.png"),
                                                        ResourcePathMake(rouletteImagePath, "menu_button_select.png"),
                                                        this, menu_selector(RouletteScene::onRoulletMenu));
    gameMenu = CCMenu::create(menuItem, NULL);
    background->addChild(gameMenu, zOderMenu);
    gameMenu->setPosition(ccp(582, 396));
    
    for (int i = 0; i < kMaxPhotoCount; i++) {
        int photoNumber = i + 1;
        CCSprite *photo = CCSprite::create(ResourcePathMake(polaroidImagePath, CCString::createWithFormat("%d.png", photoNumber)->getCString()));
        background->addChild(photo, zOderPhoto + photoNumber, zOderPhoto + photoNumber);
        setPhotoPositon(photo, photoNumber);
    }

    menuBack = CCSprite::create(ResourcePathMake(rouletteImagePath, "menu_back.png"));
    menuItem->addChild(menuBack, -1, -1);
    menuBack->setPosition(ccp(menuItem->getContentSize().width/2.0, menuItem->getContentSize().height/2.0));
    menuBack->setScale(0);
    
    CCMenuItemImage *menuHome = CCMenuItemImage::create(ResourcePathMake(rouletteImagePath, "menu_home.png"),
                                                        NULL, this, menu_selector(RouletteScene::onHome));
    menuHome->setPosition(ccp(menuBack->getContentSize().width/2.0, menuBack->getContentSize().height));
    menuHome->setRotation(90.0);
    
    CCMenuItemImage *menuHart = CCMenuItemImage::create(ResourcePathMake(rouletteImagePath, "menu_heart.png"),
                                                        NULL, this, menu_selector(RouletteScene::onHeart));
    menuHart->setPosition(ccp(menuBack->getContentSize().width/8.0, menuBack->getContentSize().height - menuBack->getContentSize().height/8.0));
    menuHart->setRotation(45.0);
    
    CCMenuItemImage *menuSearch = CCMenuItemImage::create(ResourcePathMake(rouletteImagePath, "menu_search.png"),
                                                        NULL, this, menu_selector(RouletteScene::onSearch));
    menuSearch->setPosition(ccp(0, menuBack->getContentSize().height/2.0));
    menuSearch->setRotation(0);
    
    CCMenuItemImage *menuMessage = CCMenuItemImage::create(ResourcePathMake(rouletteImagePath, "mene_message.png"),
                                                        NULL, this, menu_selector(RouletteScene::onMessage));
    menuMessage->setPosition(ccp(menuBack->getContentSize().width/8.0, menuBack->getContentSize().height/8.0));
    menuMessage->setRotation(-45.0);
    
    CCMenuItemImage *menuOption = CCMenuItemImage::create(ResourcePathMake(rouletteImagePath, "menu_option.png"),
                                                        NULL, this, menu_selector(RouletteScene::onOption));
    menuOption->setPosition(ccp(menuBack->getContentSize().width/2.0, 0));
    menuOption->setRotation(-90.0);
    
    gameSubMenu = CCMenu::create(menuHome, menuHart, menuSearch, menuMessage, menuOption, NULL);
    menuBack->addChild(gameSubMenu, 11, 11);
    gameSubMenu->setPosition(ccp(0,0));
    
    // Charater
    {
        CCSprite *manBody = CCSprite::create(ResourcePathMake(rouletteImagePath, "man.png"));
        background->addChild(manBody, zOderCharacter);
        manBody->setPosition(ccp(60, 840));
        manBody->runAction(CCRepeatForever::create(CCJumpBy::create(1.0, ccp(0, 0), 5, 2)));
        
        /*
        CCSprite *manBody = CCSprite::create(ResourcePathMake(rouletteImagePath, "man_body.png"));
        background->addChild(manBody, zOderCharacter);
        manBody->setPosition(ccp(60, 840));
        
        CCSprite *manHead = CCSprite::create(ResourcePathMake(rouletteImagePath, "man_head.png"));
        manBody->addChild(manHead, -1);
        manHead->setPosition(ccp(manBody->getContentSize().width/2, manBody->getContentSize().height - 10.0));
        manHead->runAction(CCRepeatForever::create(CCSequence::create(CCMoveTo::create(0.2, ccp(manHead->getPositionX()-1, manHead->getPositionY()-1)),
                                                                      CCRotateTo::create(0.2, -15),
                                                                      CCMoveTo::create(0.2, ccp(manHead->getPositionX()+1, manHead->getPositionY()+1)),
                                                                      CCRotateTo::create(0.0, 0),
                                                                      CCMoveTo::create(0.2, ccp(manHead->getPositionX()+1, manHead->getPositionY()-1)),
                                                                      CCRotateTo::create(0.2, 15),
                                                                      CCMoveTo::create(0.2, ccp(manHead->getPositionX()-1, manHead->getPositionY()+1)),
                                                                      CCRotateTo::create(0.0, 0),
                                                                      NULL)));*/
        
        CCSprite *tinglog = CCSprite::create(ResourcePathMake(rouletteImagePath, "ting_slogo.png"));
        manBody->addChild(tinglog);
        tinglog->setPosition(ccp(30, 140));
        
        CCSprite *womanBody = CCSprite::create(ResourcePathMake(rouletteImagePath, "woman_body.png"));
        background->addChild(womanBody, zOderCharacter);
        womanBody->setPosition(ccp(580, 830));
        
        CCSprite *womanHead = CCSprite::create(ResourcePathMake(rouletteImagePath, "woman_head.png"));
        womanBody->addChild(womanHead, -1);
        womanHead->setPosition(ccp(womanBody->getContentSize().width/2, womanBody->getContentSize().height - 10.0));
        womanHead->runAction(CCRepeatForever::create(CCSequence::create(CCRotateTo::create(0.5, -15),
                                                                        CCRotateTo::create(0.5, 15),
                                                                        NULL)));
        
        CCSprite *hart1 = CCSprite::create(ResourcePathMake(rouletteImagePath, "hart_01.png"));
        womanBody->addChild(hart1);
        hart1->setPosition(ccp(-5, 110));
        
        CCSprite *hart2 = CCSprite::create(ResourcePathMake(rouletteImagePath, "hart_02.PNG"));
        womanBody->addChild(hart2);
        hart2->setPosition(ccp(0, 135));
        
        CCSprite *funyMan = CCSprite::create(ResourcePathMake(rouletteImagePath, "fun_01.png"));
        background->addChild(funyMan, zOderPanLayer+1);
        funyMan->setPosition(ccp(80, 420));
        
        CCSprite *cloud = CCSprite::create(ResourcePathMake(rouletteImagePath, "fun_cloud.png"));
        funyMan->addChild(cloud, -1);
        cloud->setPosition(ccp(20, cloud->getContentSize().height/2.0));
    }
    
    return true;
}

void RouletteScene::setPhotoPositon(CCSprite* photo, int nPhotoNumber)
{
    CommonUtils::sharedUtils()->resizeSprite(photo, 126.0, 146.0);
    
    CCSprite *polaroid = CCSprite::create(ResourcePathMake(polaroidImagePath, "polaroid.png"));
    background->addChild(polaroid, zOderPolaroid+nPhotoNumber, zOderPolaroid+nPhotoNumber);
    
    CCSprite *gray = CCSprite::create(ResourcePathMake(polaroidImagePath, "polaroid_unvisible.png"));
    polaroid->addChild(gray, 11, 11);
    gray->setPosition(ccp(72, 107));
    int rand = arc4random() % kShowPhotoDelay;
    gray->runAction(CCSequence::create(CCDelayTime::create(rand), CCFadeOut::create(3.0), NULL));
    
    CCSprite *postIt = CCSprite::create(ResourcePathMake(polaroidImagePath, CCString::createWithFormat("post_0%d.png", nPhotoNumber)->getCString()));
    polaroid->addChild(postIt, 12, 12);
    postIt->setPosition(ccp(30, 158));
    
    // rotation
    switch (nPhotoNumber)
    {
        case 1:
            photo->setRotation(-3);
            polaroid->setRotation(-3);
            break;
        case 2:
            photo->setRotation(2);
            polaroid->setRotation(2);
            break;
        case 3:
            photo->setRotation(-3);
            polaroid->setRotation(-3);
            break;
        case 4:
            photo->setRotation(4);
            polaroid->setRotation(4);
            break;
        case 5:
            photo->setRotation(2);
            polaroid->setRotation(2);
            break;
        case 6:
            photo->setRotation(-1);
            polaroid->setRotation(-1);
            break;
        case 7:
            photo->setRotation(2);
            polaroid->setRotation(2);
            break;
        case 8:
            photo->setRotation(-3);
            polaroid->setRotation(-3);
            break;
    }

    // Position
    float nPolaroidX = 0.0f;
    float nPolaroidY = 0.0f;
    if (nPhotoNumber > kMaxPhotoCount/2)
    {
        nPhotoNumber -=  kMaxPhotoCount/2;
        nPolaroidY = 85.0;
    }
    else
    {
        nPolaroidY = 255.0;
    }
    
    nPolaroidX = 150.0 * (float)nPhotoNumber - 50.0;
    photo->setPosition(ccp(nPolaroidX - 4, nPolaroidY + 4));
    polaroid->setPosition(ccp(nPolaroidX, nPolaroidY));
    
}

void RouletteScene::showRouletteResultPopup(int nPhotoNumber)
{
    CCSprite *photo = dynamic_cast<CCSprite*>(background->getChildByTag(zOderPhoto + nPhotoNumber));
    CCSprite *polaroid = dynamic_cast<CCSprite*>(background->getChildByTag(zOderPolaroid + nPhotoNumber));
    
    if(photo) photo->setVisible(false);
    if(polaroid) polaroid->setVisible(false);
    
    RouletteResultLayer *resultLayer = RouletteResultLayer::create();
    if (resultLayer) {
        addChild(resultLayer, zOderResultLayer ,zOderResultLayer);
        resultLayer->showResultPopup(nPhotoNumber, polaroid->getPosition());
    }
}

void RouletteScene::setPhotoVisible(bool isVisible)
{
    for (int i = 0; i < kMaxPhotoCount; i++)
    {
        int nPhotoNumber = i + 1;
        CCSprite *photo = dynamic_cast<CCSprite*>(background->getChildByTag(zOderPhoto + nPhotoNumber));
        CCSprite *polaroid = dynamic_cast<CCSprite*>(background->getChildByTag(zOderPolaroid + nPhotoNumber));
        
        if(photo) photo->setVisible(true);
        if(polaroid) polaroid->setVisible(true);
    }
}

#pragma mark - OnMenu
void RouletteScene::onRoulletMenu(CCObject* pSender)
{
    if (menuBack->getScale() > 0) {
        
        float fDelayTime = 0.2;
        CCObject *obj = NULL;
        CCArray *menuChilds = gameSubMenu->getChildren();
        CCARRAY_FOREACH(menuChilds, obj)
        {
            CCNode *menuChild = dynamic_cast<CCNode*>(obj);
            menuChild->runAction(CCEaseBackIn::create(CCScaleTo::create(fDelayTime, 0.0)));
            fDelayTime = fDelayTime + 0.1;
        }
        
        menuBack->runAction(CCSequence::create(CCDelayTime::create(0.2),
                                               CCEaseBackIn::create(CCScaleTo::create(fDelayTime, 0.0)),
                                               NULL));
        
    }
    else
    {
        menuBack->runAction(CCEaseBackOut::create(CCScaleTo::create(0.2, 1.0)));
        
        float fDelayTime = 0.2;
        CCObject *obj = NULL;
        CCArray *menuChilds = gameSubMenu->getChildren();
        CCARRAY_FOREACH(menuChilds, obj)
        {
            CCNode *menuChild = dynamic_cast<CCNode*>(obj);
            menuChild->setScale(0.0);
            menuChild->runAction(CCSequence::create(CCDelayTime::create(0.2),
                                                    CCEaseBackOut::create(CCScaleTo::create(fDelayTime, 1.0)),
                                                    NULL));
            fDelayTime = fDelayTime + 0.1;
        }
    }
}

void RouletteScene::onHome(CCObject* pSender)
{
    CCDirector::sharedDirector()->replaceScene(TitleScene::scene());
}

void RouletteScene::onHeart(CCObject* pSender)
{
    
}

void RouletteScene::onSearch(CCObject* pSender)
{
    
}

void RouletteScene::onMessage(CCObject* pSender)
{
    
}

void RouletteScene::onOption(CCObject* pSender)
{
    
}

#pragma mark - Touch Event
//void RouletteScene::registerWithTouchDispatcher()
//{
//	CCDirector::sharedDirector()->getTouchDispatcher()->addTargetedDelegate(this, 0, true);
//}

bool RouletteScene::ccTouchBegan(cocos2d::CCTouch* touch, cocos2d::CCEvent* event)
{
	return true;
}

void RouletteScene::ccTouchMoved(cocos2d::CCTouch* touch, cocos2d::CCEvent* event)
{
    
}

void RouletteScene::ccTouchEnded(cocos2d::CCTouch* touch, cocos2d::CCEvent* event)
{

}

void RouletteScene::ccTouchCancelled(cocos2d::CCTouch* touch, cocos2d::CCEvent* event)
{
    
}


