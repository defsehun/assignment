#ifndef __ROULETTE_SCENE_H__
#define __ROULETTE_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class RouletteScene
:public CCLayer
{
private:
    CCSprite *background;
    CC_SYNTHESIZE(CCMenu *, gameMenu, GameMenu);
    CCMenu *gameSubMenu;
    CCSprite *menuBack;
    
    void onRoulletMenu(CCObject* pSender);
    void setPhotoPositon(CCSprite* photo, int nPhotoNumber);
    
    void onHome(CCObject* pSender);
    void onHeart(CCObject* pSender);
    void onSearch(CCObject* pSender);
    void onMessage(CCObject* pSender);
    void onOption(CCObject* pSender);
    
    //virtual void registerWithTouchDispatcher();
    virtual bool ccTouchBegan(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
    virtual void ccTouchEnded(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
    virtual void ccTouchCancelled(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
    virtual void ccTouchMoved(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
    
public:
    virtual bool init();
    static CCScene* scene();
    
    CREATE_FUNC(RouletteScene);
    
    void showRouletteResultPopup(int nPhotoNumber);
    void setPhotoVisible(bool isVisible);
	
};

#endif // __ROULETTE_SCENE_H__
