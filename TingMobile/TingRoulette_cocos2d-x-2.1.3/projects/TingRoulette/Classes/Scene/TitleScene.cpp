#include "TitleScene.h"
#include "CommonDefine.h"
#include "RouletteScene.h"

#define zOderPanLayer       1001
#define zOderCharacter      1000
#define zOderPhoto          1000
#define zOderPolaroid       2000
#define zOderResultLayer    3000

#define zOderMenu           4000

#define kShowPhotoDelay     5

CCScene* TitleScene::scene()
{
    CCScene *scene = CCScene::create();
    CCLayer *layer = TitleScene::create();
    scene->addChild(layer);
    
    return scene;
}

bool TitleScene::init()
{
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    setTouchPriority(kCCMenuHandlerPriority);
    
	CCBReader * ccbReader = new CCBReader(CCNodeLoaderLibrary::sharedCCNodeLoaderLibrary(), NULL, NULL, NULL);
    ccbReader->autorelease();
	background = ccbReader->readNodeGraphFromFile(ResourcePathMake(CCB_FOLDER_PATH, "titleScene.ccbi"),this);
	this->addChild(background);
    
    runAction(CCSequence::create(CCDelayTime::create(3.0), CCCallFunc::create(this, callfunc_selector(TitleScene::showFindHeart)), NULL));
    
    return true;
}

void TitleScene::showFindHeart(void)
{
    findHeart = CCSprite::create(ResourcePathMake(TITLE_IMAGE_PATH, "main_back.png"));
    background->addChild(findHeart);
    findHeart->setPosition(ccp(background->getContentSize().width/2.0, - findHeart->getContentSize().height/2.0));
    
    CCMenuItemImage *search = CCMenuItemImage::create(ResourcePathMake(TITLE_IMAGE_PATH, "search.PNG"),
                                                      ResourcePathMake(TITLE_IMAGE_PATH, "search.PNG"),
                                                      this, menu_selector(TitleScene::onSearch));
    CCMenu *searchMenu = CCMenu::create(search, NULL);
    findHeart->addChild(searchMenu);
    searchMenu->setPosition(ccp(findHeart->getContentSize().width - search->getContentSize().width, search->getContentSize().height + 20));
    
    CCMenuItemImage *camera = CCMenuItemImage::create(ResourcePathMake(TITLE_IMAGE_PATH, "camera.png"),
                                                      ResourcePathMake(TITLE_IMAGE_PATH, "camera.png"),
                                                      this, menu_selector(TitleScene::onCamera));
    
    CCSprite *cameraLight = CCSprite::create(ResourcePathMake(TITLE_IMAGE_PATH, "camera_light.PNG"));
    ccBlendFunc bf = {GL_ONE, GL_ONE};
    cameraLight->setBlendFunc(bf);
    camera->addChild(cameraLight, 11, 11);
    cameraLight->setPosition(ccp(camera->getContentSize().width/2.0 - 10, camera->getContentSize().height/2.0));
    cameraLight->setOpacity(0);
    
    CCMenu *cameraMenu = CCMenu::create(camera, NULL);
    findHeart->addChild(cameraMenu);
    cameraMenu->setPosition(ccp(camera->getContentSize().width/2.0 + 5, findHeart->getContentSize().height - camera->getContentSize().height/3.0));
    
    findHeart->runAction(CCEaseExponentialOut::create(CCMoveTo::create(1.0, ccp(background->getContentSize().width/2.0, findHeart->getContentSize().height/2.0))));
}

void TitleScene::onSearch(CCObject *pSender)
{
    CCDirector::sharedDirector()->replaceScene(RouletteScene::scene());
}

void TitleScene::onCamera(CCObject *pSender)
{
    CCMenuItemImage *camera = dynamic_cast<CCMenuItemImage*>(pSender);
    
    if (camera) {
        CCSprite *cameraLight = dynamic_cast<CCSprite *>(camera->getChildByTag(11));
        if(cameraLight) {
            cameraLight->runAction(CCSequence::create(CCFadeIn::create(0.1),
                                                      //CCEaseElasticOut::create(CCScaleTo::create(0.25, 2.0)),
                                                      //CCEaseElasticIn::create(CCScaleTo::create(0.25, 1.0)),
                                                      CCFadeOut::create(0.1), NULL));
            createPhoto(camera);
            
        }
    }
   
}


void TitleScene::createPhoto(CCObject *pSender)
{
    CCSprite *photo = CCSprite::create(ResourcePathMake(TITLE_IMAGE_PATH, "ting_photo.png"));
    CCMenuItemImage *camera = dynamic_cast<CCMenuItemImage*>(pSender);
    
    if (camera)
    {
        int gapX = (arc4random() % 10) * 10;
        int gapY = (arc4random() % 20) * 10;
        
        camera->addChild(photo, -1);
        photo->setPosition(ccp(camera->getContentSize().width/2.0, camera->getContentSize().height/2.0));
        photo->setRotation(60);
        
        ccBezierConfig bezier;
        bezier.controlPoint_1 = ccp(photo->getPositionX() + 50, photo->getPositionY() + 30);
        bezier.controlPoint_2 = ccp(photo->getPositionX() + 300, photo->getPositionY() + 200);
        bezier.endPosition = ccp((findHeart->getContentSize().width - photo->getContentSize().width) + gapX, -photo->getContentSize().width + gapY);
        
        photo->runAction(CCSequence::create(CCDelayTime::create(0.5), CCRotateTo::create(0.5, 700 + gapX), NULL));
        photo->runAction(CCSequence::create(CCEaseExponentialOut::create(CCMoveTo::create(0.2, ccp(photo->getPositionX() + 50, photo->getPositionY() + 30))),
                                            CCDelayTime::create(0.1),
                                            CCBezierTo::create(0.8, bezier),
                                            CCCallFuncO::create(this, callfuncO_selector(TitleScene::createPhotoAfterShowCharater), (photo)),
                                            NULL));
        
        photoCount++;
        
    }
    
}

void TitleScene::createPhotoAfterShowCharater(CCObject *pSender)
{
    CCSprite *photo = dynamic_cast<CCSprite*>(pSender);
    
    if (photo) {
        int faceNum = arc4random() % 4 + 1;
        CCSprite *face = CCSprite::create(ResourcePathMake(TITLE_IMAGE_PATH, CCString::createWithFormat("face_00%d.png", faceNum)->getCString()));
        photo->addChild(face);
        face->setPosition(ccp(photo->getContentSize().width/2.0, photo->getContentSize().height/2.0 + 15));
        face->setRotation(photo->getRotation());
        face->setOpacity(0);
        face->runAction(CCFadeIn::create(1.0));
    }
    
}





