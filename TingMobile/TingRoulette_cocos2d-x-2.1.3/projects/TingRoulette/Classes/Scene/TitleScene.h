#ifndef __TITLE_SCENE_H__
#define __TITLE_SCENE_H__

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class TitleScene
:public CCLayer
{
private:
    CCNode *background;
    int photoCount;
    CCSprite *findHeart;
    
    void showFindHeart(void);
    void onCamera(CCObject *pSender);
    void onSearch(CCObject *pSender);
    
    void createPhoto(CCObject *pSender);
    void createPhotoAfterShowCharater(CCObject *pSender);
    
public:
    virtual bool init();
    static CCScene* scene();
    
    CREATE_FUNC(TitleScene);
	
};

#endif // __TITLE_SCENE_H__
