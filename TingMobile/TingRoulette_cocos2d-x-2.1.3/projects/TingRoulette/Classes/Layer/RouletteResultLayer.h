
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class RouletteResultLayer
:public CCLayerColor
{
private:
    
    bool isMenuClicked;
    
    CCMenu *popupMenu;
    
    CCPoint orgPopupPositon;
    
    CCSprite *photoFrame;
    CCSprite *photo;
    CCSprite *title;
    CCSprite *glowEffect;
    
    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
    
    void rouletteRetry(CCObject *sender);
    void addFriend(CCObject *sender);
    void callAfterPhotoAction();
    void photoParticle();
    void callAfterReversePhotoAction();
    
public:
    virtual bool init();
    CREATE_FUNC(RouletteResultLayer);
    
    void showResultPopup(int nPhotoNumber, CCPoint popupPosition);
    
};

