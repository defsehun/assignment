#include "RoulettePanLayer.h"
#include "CommonDefine.h"
#include "RouletteScene.h"

#define kPanWidth   603.0f
#define kPanHeight  603.0f

#define panRotateTime 5.0f

bool RoulettePanLayer::init()
{
    if ( !CCLayer::init() )
    {
        return false;
    }
    
    setTouchEnabled(true);
    setTouchMode(kCCTouchesOneByOne);
    
    setContentSize(CCSizeMake(kPanWidth, kPanHeight));
    
    roulettePan = CCSprite::create(ResourcePathMake(rouletteImagePath, "inner_pan.PNG"));
    addChild(roulettePan);
    
    CCSprite *backPan = CCSprite::create(ResourcePathMake(rouletteImagePath, "back_pan.png"));
    roulettePan->addChild(backPan);
    backPan->setPosition(ccp(280.2, 279.4));
    
    rouletteArrow = CCSprite::create(ResourcePathMake(rouletteImagePath, "rolling.PNG"));
    addChild(rouletteArrow);
    rouletteArrow->setPosition(ccp(-3, -3));
    rouletteArrow->setAnchorPoint(ccp(0.5, 0.4));
    
    CCSprite *hart = CCSprite::create(ResourcePathMake(rouletteImagePath, "hart_arrow.PNG"));
    rouletteArrow->addChild(hart, 2);
    hart->setPosition(ccp(43, -21));

    CCSprite *frontPan = CCSprite::create(ResourcePathMake(rouletteImagePath, "front_pan.png"));
    addChild(frontPan);
    frontPan->setPosition(ccp(-3, -3));
    
    return true;
}

#pragma mark - Touch Event
bool RoulettePanLayer::ccTouchBegan(cocos2d::CCTouch* touch, cocos2d::CCEvent* event)
{
    CCPoint location = CCDirector::sharedDirector()->convertToGL(touch->getLocationInView());
    float bottomHeight = CCDirector::sharedDirector()->getWinSize().height - kPanHeight;
    
    if(bPanDrag == false && location.y > bottomHeight)
    {
        touchStartLocation = location;
        prevLocation = location;
        bPanDrag = true;
        time(&dragStartTime);
    }
    
	return true;
}

void RoulettePanLayer::ccTouchMoved(cocos2d::CCTouch* touch, cocos2d::CCEvent* event)
{
    if(bPanDrag)
    {
        CCPoint location = CCDirector::sharedDirector()->convertToGL(touch->getLocationInView());
        float j = 180.0f/kPanHeight;
        
        if (kPanWidth/2.0f > location.x)
        {
            if (prevLocation.y > location.y)
            {
                float k = prevLocation.y - location.y;
                float n = j * k;
                roulettePan->setRotation(roulettePan->getRotation() - n);
                rouletteArrow->setRotation(rouletteArrow->getRotation() + n);
                nPanRotateType = 1;
            }
            else if (prevLocation.y < location.y)
            {
                float k = location.y - prevLocation.y;
                float n = j * k;
                roulettePan->setRotation(roulettePan->getRotation() + n);
                rouletteArrow->setRotation(rouletteArrow->getRotation() - n);
                nPanRotateType = 0;
            }
            else
            {
                float bottomHeight = CCDirector::sharedDirector()->getWinSize().height - kPanHeight;
                
                if (kPanHeight/2.0f < (location.y - bottomHeight))
                {
                    float k = prevLocation.x - location.x;
                    float n = j * k;
                    roulettePan->setRotation(roulettePan->getRotation() - n);
                    rouletteArrow->setRotation(rouletteArrow->getRotation() + n);
                    nPanRotateType = 0;
                }
                else if (kPanHeight/2.0f > (location.y - bottomHeight))
                {
                    float k = location.x - prevLocation.x;
                    float n = j * k;
                    roulettePan->setRotation(roulettePan->getRotation() - n);
                    rouletteArrow->setRotation(rouletteArrow->getRotation() + n);
                    nPanRotateType = 1;
                }
            }
        }
        else if (kPanWidth/2.0f < location.x)
        {
            if (prevLocation.y > location.y)
            {
                float k = prevLocation.y - location.y;
                float n = j * k;
                roulettePan->setRotation(roulettePan->getRotation() + n);
                rouletteArrow->setRotation(rouletteArrow->getRotation() - n);
                nPanRotateType = 0;
            }
            else if (prevLocation.y < location.y)
            {
                float k = location.y - prevLocation.y;
                float n = j * k;
                roulettePan->setRotation(roulettePan->getRotation() - n);
                rouletteArrow->setRotation(rouletteArrow->getRotation() + n);
                nPanRotateType = 1;
            }
            else
            {
                float bottomHeight = CCDirector::sharedDirector()->getWinSize().height - kPanHeight;
                
                if (kPanHeight/2.0f > (location.y - bottomHeight))
                {
                    float k = prevLocation.x - location.x;
                    float n = j * k;
                    roulettePan->setRotation(roulettePan->getRotation() + n);
                    rouletteArrow->setRotation(rouletteArrow->getRotation() - n);
                    nPanRotateType = 0;
                }
                else if (kPanHeight/2.0f < (location.y - bottomHeight))
                {
                    float k = location.x - prevLocation.x;
                    float n = j * k;
                    roulettePan->setRotation(roulettePan->getRotation() + n);
                    rouletteArrow->setRotation(rouletteArrow->getRotation() - n);
                    nPanRotateType = 0;
                }
            }
        }
        
        prevLocation = location;
    }
}

void RoulettePanLayer::ccTouchEnded(cocos2d::CCTouch* touch, cocos2d::CCEvent* event)
{
	if(bPanDrag)
	{
        time_t dragEndTime;
        time(&dragEndTime);
        
        float fTimeCondition = dragEndTime - dragStartTime;
        
        CCPoint location = CCDirector::sharedDirector()->convertToGL(touch->getLocationInView());
        
        if (fTimeCondition < 1.5)
        {
            if (touchStartLocation.y + (kPanHeight/10.0f) < location.y)
            {
                float distance = location.y - touchStartLocation.y;
                panRotationalAction(nPanRotateType, fTimeCondition, distance);
            }
            else if(touchStartLocation.y - (kPanHeight/10.0f) > location.y)
            {
                int distance = touchStartLocation.y - location.y;
                panRotationalAction(nPanRotateType, fTimeCondition, distance);
            }

        }
                
        bPanDrag = false;
	}
}

void RoulettePanLayer::ccTouchCancelled(cocos2d::CCTouch* touch, cocos2d::CCEvent* event)
{
    
}

void RoulettePanLayer::panRotationalAction(int type, float fSecond, float distance)
{
//    float j = 180.0f/kPanHeight;
//    float k = distance / second;
//    float fDeltaAngle = j*k + 360.0f;
    
    //test
    nTargetNumber = arc4random() % kMaxPhotoCount + 1;
    CCLog("panRotationalAction : %d", nTargetNumber);
    
    // 시작화살표가 5부터임
    // 화살표는 기본 5바퀴 반, 팬은 5바퀴
    float fDefaultRotation = 720.0 * panRotateTime;
    
    float fPanDeltaAngle = fDefaultRotation;
    float fArrowDeltaAngle = fDefaultRotation + 180.0;
    float fArrowTagetAngel = (float)(nTargetNumber-1) * 45.0; // 이동할 목표점
    
    CCActionInterval* panRotate;
    CCActionInterval* arrowRotate;
    
    if (type == 0) // 0은 팬이 시계방향 ,1은 시계반대방향
    {
        fPanDeltaAngle = fPanDeltaAngle - fmodf(roulettePan->getRotation(), 360.0);
        fArrowDeltaAngle = fArrowDeltaAngle + fmodf(rouletteArrow->getRotation(), 360.0);
        
        panRotate = CCRotateBy::create(panRotateTime, fPanDeltaAngle);
        arrowRotate = CCRotateBy::create(panRotateTime, -(fArrowDeltaAngle - fArrowTagetAngel));
    }
    else
    {
        fPanDeltaAngle = fPanDeltaAngle + fmodf(roulettePan->getRotation(), 360.0);
        fArrowDeltaAngle = fArrowDeltaAngle - fmodf(rouletteArrow->getRotation(), 360.0);
        
        panRotate = CCRotateBy::create(panRotateTime, -fPanDeltaAngle);
        arrowRotate = CCRotateBy::create(panRotateTime, fArrowDeltaAngle + fArrowTagetAngel);
    }
    
    CCActionInterval* pan_rotate_ease_out = CCEaseSineOut::create((CCActionInterval*)(panRotate->copy()->autorelease()) );
    CCActionInterval* arrow_rotate_ease_out = CCEaseSineOut::create((CCActionInterval*)(arrowRotate->copy()->autorelease()) );
    
    CCSequence* panSeq = CCSequence::create(pan_rotate_ease_out, NULL);
    CCSequence* arrowSeq = CCSequence::create(arrow_rotate_ease_out,
                                              CCCallFunc::create(this, callfunc_selector(RoulettePanLayer::callAfterPanRotationalAction)),
                                              NULL);
    
    roulettePan->runAction(panSeq);
    rouletteArrow->runAction(arrowSeq);
    
    setTouchEnabled(false);
    
    // game menu touch lock
    CCNode *runningNode = (CCNode *)CCDirector::sharedDirector()->getRunningScene()->getChildren()->objectAtIndex(0);
	RouletteScene *scene = dynamic_cast<RouletteScene*>(runningNode);
    scene->getGameMenu()->setEnabled(false);
    
}

void RoulettePanLayer::callAfterPanRotationalAction()
{
    CCSprite *glowEffect = CCSprite::create(ResourcePathMake(rouletteImagePath, "pan_light.png"));
	glowEffect->setScale(1.0);
	ccBlendFunc bf = {GL_ONE, GL_ONE};
	glowEffect->setBlendFunc(bf);
    glowEffect->setOpacity(0);
	glowEffect->runAction(CCSequence::create(CCFadeTo::create(0.4, 150),
                                             CCScaleTo::create(0.3, 1.2),
                                             CCScaleTo::create(0.3, 1.0),
                                             CCDelayTime::create(0.4),
                                             CCCallFunc::create(this, callfunc_selector(RoulettePanLayer::endPanRotationalAction)),
                                             NULL));
    rouletteArrow->addChild(glowEffect, 1, 1);
    glowEffect->setPosition(ccp(rouletteArrow->getContentSize().width/2.0, -90));
    
}

void RoulettePanLayer::endPanRotationalAction()
{
    setTouchEnabled(true);
    rouletteArrow->removeChildByTag(1);

    CCNode *runningNode = (CCNode *)CCDirector::sharedDirector()->getRunningScene()->getChildren()->objectAtIndex(0);
	RouletteScene *scene = dynamic_cast<RouletteScene*>(runningNode);
    scene->getGameMenu()->setEnabled(true);
    scene->showRouletteResultPopup(nTargetNumber);
}



