#include "RouletteResultLayer.h"
#include "CommonDefine.h"
#include "CommonUtils.h"
#include "RouletteScene.h"

bool RouletteResultLayer::init()
{
    if(!CCLayerColor::initWithColor(ccc4(0, 0, 0, 100)))
        return false;
    
    CCLog("RouletteResultLayer::init()");
    
    setTouchEnabled(true);
    setTouchMode(kCCTouchesOneByOne);
    setTouchPriority(kCCMenuHandlerPriority-999);
    
    photoFrame = CCSprite::create(ResourcePathMake(rouletteResultImagePath, "photo_frame.png"));
    addChild(photoFrame, 2);
    photoFrame->setPosition(ccp(getContentSize().width/2.0, getContentSize().height/2.0));
    
//    CCSprite *star1 = CCSprite::create(ResourcePathMake(rouletteResultImagePath, "star_01.png"));
//    photoFrame->addChild(star1, 0, 0);
//    star1->setPosition(ccp(0, 0));
//    
//    CCSprite *star2 = CCSprite::create(ResourcePathMake(rouletteResultImagePath, "star_02.png"));
//    photoFrame->addChild(star2, 1, 1);
//    star2->setPosition(ccp(photoFrame->getContentSize().width, photoFrame->getContentSize().height));
    
    CCSprite *star3 = CCSprite::create(ResourcePathMake(rouletteResultImagePath, "star_03.png"));
    photoFrame->addChild(star3);
    star3->setPosition(ccp(0, photoFrame->getContentSize().height));
    
    CCSprite *star4 = CCSprite::create(ResourcePathMake(rouletteResultImagePath, "star_04.png"));
    photoFrame->addChild(star4);
    star4->setPosition(ccp(photoFrame->getContentSize().width, 0));
    
    title = CCSprite::create(ResourcePathMake(rouletteResultImagePath, "congratulation.png"));
    photoFrame->addChild(title);
    title->setPosition(ccp(photoFrame->getContentSize().width/2.0, photoFrame->getContentSize().height + title->getContentSize().height));
    
    CCMenuItemImage *retryItem = CCMenuItemImage::create(ResourcePathMake(rouletteResultImagePath, "btn_retry.png"),
                                                         ResourcePathMake(rouletteResultImagePath, "btn_retry_select.png"),
                                                         this, menu_selector(RouletteResultLayer::rouletteRetry));
                                                         
    CCMenuItemImage *addFriendItem = CCMenuItemImage::create(ResourcePathMake(rouletteResultImagePath, "btn_Friend.png"),
                                                             ResourcePathMake(rouletteResultImagePath, "btn_Friend_select.png"),
                                                             this, menu_selector(RouletteResultLayer::addFriend));
                                                             
    popupMenu = CCMenu::create(retryItem, addFriendItem, NULL);
    photoFrame->addChild(popupMenu);
    popupMenu->alignItemsHorizontallyWithPadding(12.0f);
    popupMenu->setPosition(ccp(photoFrame->getContentSize().width/2.0, 0));
    
    return true;
}

#pragma mark - Touch Event
bool RouletteResultLayer::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
    isMenuClicked = popupMenu->ccTouchBegan(pTouch, pEvent);
    
    // block touch event
    return true;
}

void RouletteResultLayer::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{
    if(isMenuClicked)
        popupMenu->ccTouchMoved(pTouch, pEvent);
}


void RouletteResultLayer::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
    if(isMenuClicked)
        popupMenu->ccTouchEnded(pTouch, pEvent);
}

void RouletteResultLayer::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{
    if(isMenuClicked)
        popupMenu->ccTouchCancelled(pTouch, pEvent);
}

#pragma mark - Photo Function
void RouletteResultLayer::showResultPopup(int nPhotoNumber, CCPoint popupPosition)
{
    orgPopupPositon = popupPosition;
    
    photo = CCSprite::create(ResourcePathMake(polaroidImagePath, CCString::createWithFormat("%d.png", nPhotoNumber)->getCString()));
    CommonUtils::sharedUtils()->resizeSprite(photo, 438.0, 588.0);
    addChild(photo, 1);
    
    float photoScaleX = photo->getScaleX();
    float photoScaleY = photo->getScaleY();
    
    photo->setScale(0.0);
    photoFrame->setVisible(false);
    photo->setPosition(popupPosition);
    photo->runAction(CCMoveTo::create(1.0, ccp(getContentSize().width/2.0, getContentSize().height/2.0)));
    photo->runAction(CCRotateTo::create(1.0, 1800));
    photo->runAction(CCSequence::create(CCScaleTo::create(1.0, photoScaleX, photoScaleY),
                                        CCCallFunc::create(this, callfunc_selector(RouletteResultLayer::callAfterPhotoAction)),
                                        NULL));
}

void RouletteResultLayer::callAfterPhotoAction()
{
    photoFrame->setVisible(true);
    photoFrame->setOpacity(0);
    photoFrame->runAction(CCFadeTo::create(0.0, 255));
    
    title->setScale(0);
    title->runAction(CCSequence::create(CCDelayTime::create(0.2),
                                        CCEaseBackOut::create(CCScaleTo::create(0.3, 1.0)),
                                        //CCJumpTo::create(0.8, title->getPosition(), 40, 2),
                                        CCCallFunc::create(this, callfunc_selector(RouletteResultLayer::photoParticle)),
                                        NULL));
    
    // start Progress
    {
        CCProgressTimer *left = CCProgressTimer::create(CCSprite::create(ResourcePathMake(rouletteResultImagePath, "star_01.png")));
        left->setType(kCCProgressTimerTypeBar);
        //    Setup for a bar starting from the left since the midpoint is 0 for the x
        left->setMidpoint(ccp(1,0));
        //    Setup for a horizontal bar since the bar change rate is 0 for y meaning no vertical change
        left->setBarChangeRate(ccp(1, 0));
        photoFrame->addChild(left);
        left->setPosition(ccp(0, 0));
        left->setRotation(-50);
        left->runAction(CCProgressTo::create(0.2, 100));
        
        CCProgressTimer *right = CCProgressTimer::create(CCSprite::create(ResourcePathMake(rouletteResultImagePath, "star_02.png")));
        right->setType(kCCProgressTimerTypeBar);
        //    Setup for a bar starting from the left since the midpoint is 1 for the x
        right->setMidpoint(ccp(0, 0));
        //    Setup for a horizontal bar since the bar change rate is 0 for y meaning no vertical change
        right->setBarChangeRate(ccp(1, 0));
        photoFrame->addChild(right);
        right->setPosition(ccp(photoFrame->getContentSize().width, photoFrame->getContentSize().height));
        right->setRotation(-40);
        right->runAction(CCProgressTo::create(0.2, 100));
    }
}

void RouletteResultLayer::photoParticle()
{
    // CCParticleExplosion
    CommonUtils::sharedUtils()->addFireWorksParticleAtNode(photo, -1, -1);
    
    glowEffect = CCSprite::create(ResourcePathMake(rouletteResultImagePath, "glowFx.png"));
	glowEffect->setScale(1.0);
	ccBlendFunc bf = {GL_ONE, GL_ONE};
	glowEffect->setBlendFunc(bf);
    glowEffect->runAction(CCRepeatForever::create(CCSequence::create(CCScaleTo::create(2.0, 1.2),
                                                                     CCDelayTime::create(0.5),
                                                                     CCScaleTo::create(2.0, 1.0),
                                                                     CCDelayTime::create(0.5),
                                                                     NULL)));
    addChild(glowEffect, 0);
    glowEffect->setPosition(ccp(getContentSize().width/2.0, getContentSize().height/2.0));
    
    
    // Star Effect
    {
        CCArray *feverEffects = CCArray::create();
        CCSprite *feverEffect = NULL;
        int posX = getContentSize().width/2;
        int posY = getContentSize().height/2;
        for (int i = 0; i < 8; i++) {
            feverEffect = CCSprite::create(ResourcePathMake(COMMON_IMAGE_PATH, "star_01.png"));
            feverEffect->setBlendFunc((ccBlendFunc){GL_ONE, GL_ONE});
            addChild(feverEffect, 0);
            feverEffects->addObject(feverEffect);
            
            switch (i) {
                case 0:
                    feverEffect->setPosition(ccp(posX - 100, posY - 200));
                    break;
                case 1:
                    feverEffect->setPosition(ccp(posX - 300, posY - 200));
                    break;
                case 2:
                    feverEffect->setPosition(ccp(posX + 300, posY - 250));
                    break;
                case 3:
                    feverEffect->setPosition(ccp(posX + 150, posY - 300));
                    break;
                case 4:
                    feverEffect->setPosition(ccp(posX - 250, posY + 300));
                    break;
                case 5:
                    feverEffect->setPosition(ccp(posX - 100, posY + 300));
                    break;
                case 6:
                    feverEffect->setPosition(ccp(posX + 300, posY + 150));
                    break;
                case 7:
                    feverEffect->setPosition(ccp(posX + 250, posY + 300));
                    break;
                    
                default:
                    break;
            }
        }
        
        CCObject *obj = NULL;
        CCSprite *effect = NULL;
        int n = 0;
        CCARRAY_FOREACH(feverEffects, obj)
        {
            effect = dynamic_cast<CCSprite*>(obj);
            effect->setVisible(true);
            effect->runAction(CCRepeatForever::create(CCSequence::create(CCDelayTime::create(n/5),
                                                                         CCRotateBy::create(2 ,360),
                                                                         CCScaleTo::create(0.25, 0.1),
                                                                         CCScaleTo::create(0.25, 2),
                                                                         CCScaleTo::create(0.25, 1), NULL)));
            n++;
        }
    }
}

void RouletteResultLayer::callAfterReversePhotoAction()
{
    CCNode *runningNode = (CCNode *)CCDirector::sharedDirector()->getRunningScene()->getChildren()->objectAtIndex(0);
	RouletteScene *scene = dynamic_cast<RouletteScene*>(runningNode);
    scene->setPhotoVisible(true);
    
    removeFromParent();
}

#pragma mark - On Menu
void RouletteResultLayer::rouletteRetry(CCObject *sender)
{
    // Photo Action
    photoFrame->setVisible(false);
    glowEffect->setVisible(false);
    
    photo->runAction(CCMoveTo::create(1.0, orgPopupPositon));
    photo->runAction(CCRotateTo::create(1.0, 1800));
    photo->runAction(CCSequence::create(CCScaleTo::create(1.0, 0),
                                        CCCallFunc::create(this, callfunc_selector(RouletteResultLayer::callAfterReversePhotoAction)),
                                        NULL));
    
}

void RouletteResultLayer::addFriend(CCObject *sender)
{
    
}



