
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class RoulettePanLayer
:public CCLayer
{
private:
    CCSprite *roulettePan;
    CCSprite *rouletteArrow;
    
    bool bPanDrag;
    int nPanRotateType; // 0:시계방향 1:반대방향
    time_t dragStartTime;
    
    CCPoint touchStartLocation;
    CCPoint prevLocation;
    
    int nTargetNumber;
    
    virtual bool ccTouchBegan(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
    virtual void ccTouchEnded(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
    virtual void ccTouchCancelled(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
    virtual void ccTouchMoved(cocos2d::CCTouch* touch, cocos2d::CCEvent* event);
    
    void panRotationalAction(int type, float fSecond, float distance);
    void callAfterPanRotationalAction();
    void endPanRotationalAction();
    
public:
    virtual bool init();
    CREATE_FUNC(RoulettePanLayer);
    
    
};

