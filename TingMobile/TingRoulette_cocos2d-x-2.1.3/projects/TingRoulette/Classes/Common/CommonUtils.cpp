//
//  CommonUtils.cpp
//
//  Created by Park Se Hun on 13. 5. 6..
//
//

#include "CommonUtils.h"
#include "CommonDefine.h"

static CommonUtils* s_pCommonUtils = NULL;

CommonUtils::CommonUtils(void)
{
    plistDict = NULL;
}

CommonUtils::~CommonUtils(void)
{
    CC_SAFE_RELEASE_NULL(plistDict);
}

CommonUtils* CommonUtils::sharedUtils()
{
    if (s_pCommonUtils == NULL)
    {
        s_pCommonUtils = new CommonUtils();
        s_pCommonUtils->init();
    }
    return s_pCommonUtils;
}

bool CommonUtils::init()
{
    
    return true;
}

#pragma mark -
#pragma mark Localizable
CCString* CommonUtils::getValueByKey(const std::string& key)
{
	CCString* value = NULL;
	if (s_pCommonUtils->plistDict == NULL) {
        
        ccLanguageType langType = CCApplication::sharedApplication()->getCurrentLanguage();
        std::string path;
        
        switch (langType) {
            case kLanguageKorean:
                path = CCFileUtils::sharedFileUtils()->fullPathForFilename("GameData/Localizable_ko.plist");
                break;
                
            default:
                path = CCFileUtils::sharedFileUtils()->fullPathForFilename("GameData/Localizable_ko.plist");
                break;
        }
        
		s_pCommonUtils->plistDict = CCDictionary::createWithContentsOfFile(path.c_str());
		s_pCommonUtils->plistDict->retain();
	}
    
	value = dynamic_cast<CCString*>(s_pCommonUtils->plistDict->objectForKey(key));
    
	// if cannot find key for string then return key string
	if(value == NULL)
		return CCStringMake(&key[0]);
    
	return value;
}

CCString* CommonUtils::getLocalizingName(const std::string& key)
{
	CCString *strkey = CCString::createWithFormat("%s_dp", key.c_str());
	
	return getValueByKey(strkey->m_sString);
}

CCString* CommonUtils::getLocalizingDescription(const std::string& key)
{
	CCString *strkey = CCString::createWithFormat("%s_desc", key.c_str());
	
	return getValueByKey(strkey->m_sString);
}

void CommonUtils::resizeSprite(CCSprite* sprite, float width, float height)
{
    sprite->setScaleX(width / sprite->getContentSize().width);
    sprite->setScaleY(height / sprite->getContentSize().height);
}

void CommonUtils::addFireWorksParticleAtNode(CCNode* node, int zorder, int tag)
{
    ccColor4F color = {0.6f, 0.6f, 0.9f, 1.0f};
    
    CCParticleExplosion *particle = CCParticleExplosion::create();
    particle->setTexture(CCTextureCache::sharedTextureCache()->addImage(ResourcePathMake(rouletteResultImagePath, "star_03.png")));
    particle->setPosition(ccp(120, 200));
    particle->setSpeed(300);
    particle->setSpeedVar(300);
    particle->setStartColor(color);
    particle->setEndColor(color);
    particle->setScale(3);
    particle->setBlendFunc((ccBlendFunc){GL_ONE, GL_ONE});
    particle->setAutoRemoveOnFinish(true);
    node->addChild(particle, zorder, tag);
    
    CCParticleExplosion *particle2 = CCParticleExplosion::create();
    particle2->setTexture(CCTextureCache::sharedTextureCache()->addImage(ResourcePathMake(rouletteResultImagePath, "star_04.png")));
    particle2->setPosition(ccp(180, 200));
    particle2->setSpeed(250);
    particle2->setSpeedVar(250);
    particle2->setStartColor(color);
    particle2->setEndColor(color);
    particle->setScale(3);
    particle2->setBlendFunc((ccBlendFunc){GL_ONE, GL_ONE});
    particle2->setAutoRemoveOnFinish(true);
    node->addChild(particle2, zorder, tag);
    
}

#pragma mark -
#pragma mark indicator
Indicator::Indicator()
{
    indicator = NULL;
}

Indicator::~Indicator()
{
    CC_SAFE_RELEASE_NULL(indicator);
}

bool Indicator::init()
{
    if (!CCLayer::init())
        return false;
    
    indicator = CCSprite::create(ResourcePathMake(COMMON_IMAGE_PATH, "Loading_Waiting_Mark.png"));
    indicator->setPosition(ccp(320, 480));
//    CCSprite* indicator = CommonUtils::sharedUtils()->activeIndicator(layer);
    
    indicator->runAction(CCRepeatForever::create(CCRotateBy::create(0.5, 270)));
    this->addChild(indicator, 9135, 9135);
    indicator->retain();
    return true;
}

void Indicator::setVisible(bool var)
{
    CCLayer::setVisible(var);
    
    if (var)
        indicator->resumeSchedulerAndActions();
    else
        indicator->pauseSchedulerAndActions();
}


