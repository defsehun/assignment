//
//  CommonUtils.h
//
//  Created by Park Se Hun on 13. 5. 6..
//
//

#ifndef __WordQuiz__CommonUtils__
#define __WordQuiz__CommonUtils__

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

NS_CC_BEGIN

class CommonUtils : public CCObject
{
private:
	CommonUtils(void);
	~CommonUtils(void);
	
    CCDictionary *plistDict;
    
public:
	static CommonUtils* sharedUtils();
	bool init();
    
    CCString* getValueByKey(const std::string& key);
    CCString* getLocalizingName(const std::string& key);
    CCString* getLocalizingDescription(const std::string& key);
    
    void resizeSprite(CCSprite* sprite, float width, float height);
    void addFireWorksParticleAtNode(CCNode* node, int zorder, int tag);
};


class Indicator:
public CCLayer
{
public:
    Indicator();
	virtual ~Indicator();
    virtual bool init();
    
    CCSprite* indicator;
    
    CREATE_FUNC(Indicator);
    
    virtual void setVisible(bool var);
};

NS_CC_END


#endif /* defined(__WordQuiz__CommonUtils__) */
